`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//Adam Satar
//team17 ece1819
//module that splits 32 bit gpio into respective control signals
//avoids having to use many, many, many 1 and 2 bit GPIO IP
//cores
//////////////////////////////////////////////////////////////////////////////////


module gpio_bridge(     
  input [31:0] gpio_in,
  input dout_0,dout_1,dout_2,dout_3,
  
  output[7:0] ctrl_0,ctrl_1,ctrl_2,ctrl_3,
  
  output [3:0] dout_readback
  
  );
  
  assign dout_readback = {dout_3,dout_2,dout_1,dout_0};
  
  wire [3:0] ps_wen,ps_next_addr,ps_set_addr,ps_write,ps_mode,ps_playback_en,ps_dout_en,ps_din;
  
  assign ps_wen = gpio_in[31:28];
  assign ps_next_addr= gpio_in[27:24];
  assign ps_set_addr= gpio_in[23:20];
  assign ps_write= gpio_in[19:16];
  assign ps_mode= gpio_in[15:12];
  assign ps_playback_en= gpio_in[11:8];
  assign ps_dout_en = gpio_in[7:4];
  assign ps_din = gpio_in [3:0];
  
  op_decoder_encoder op_dec_en
  
  (
    .ps_wen(ps_wen),
    .ps_next_addr(ps_next_addr),
    .ps_set_addr(ps_set_addr),
    .ps_write(ps_write),
    .ps_mode(ps_mode),
    .ps_playback_en(ps_playback_en),
    .ps_dout_en(ps_dout_en),
    .ps_din(ps_din),
    .ctrl_0(ctrl_0),
    .ctrl_1(ctrl_1),
    .ctrl_2(ctrl_2),
    .ctrl_3(ctrl_3)
    );
    
    
    
    endmodule
