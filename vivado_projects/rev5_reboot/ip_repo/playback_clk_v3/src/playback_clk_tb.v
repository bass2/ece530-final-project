`timescale 1ns / 1ps



module playback_clk_tb();


localparam SYSCLK = 30;

//inputs

reg s_axi_clk = 0; reg s_axi_reset = 0; ;reg hs_clock = 0;
reg [3:0] gpio_in=0;
reg [31:0] frequency=0;

//outputs
wire ch_0,ch_1,ch_2,ch_3;
/*

module playback_clk  #(parameter SYSCLK = 100000000)
(input s_axi_clk,s_axi_reset,hs_clock,
//input [1:0] ch_num,
//input write,enable,
input [3:0] gpio_in,
input [31:0] frequency,
output ch_0,ch_1,ch_2,ch_3                                      

   );
*/

playback_clk #(.SYSCLK (SYSCLK)) 

uut (
    .s_axi_clk(s_axi_clk),
    .s_axi_reset(s_axi_reset),
    .hs_clock(hs_clock),
    .gpio_in(gpio_in),
    .frequency(frequency),
    .ch_0(ch_0),
    .ch_1(ch_1),
    .ch_2(ch_2),
    .ch_3(ch_3)
);
//write,en,ch,ch
initial fork
    #3 s_axi_reset = 1;
    #4 frequency = 6;
    #5 gpio_in = 4'b1000;
       #8 gpio_in = 4'b0000;
          #11 gpio_in = 4'b1001;
             #14 gpio_in = 4'b0001;
                #17 gpio_in = 4'b1010;
                   #20 gpio_in = 4'b0010;
                      #23 gpio_in = 4'b1011;
                      #26 gpio_in = 4'b0011;
                      #30 gpio_in = 4'b0100; 
                                  #33 gpio_in = 4'b0101;
                                            #36 gpio_in = 4'b0110;
                                                        #39 gpio_in = 4'b0111;
                                                      
                //      #29 gpio_in = 4'b0011;
    //#10 gpio_in = 4'b0111;
    #50 $stop;
join

always #1 s_axi_clk = ~s_axi_clk;
always #1 hs_clock = ~hs_clock;
endmodule
