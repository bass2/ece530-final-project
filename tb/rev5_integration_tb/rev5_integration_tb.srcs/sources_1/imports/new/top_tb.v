`timescale 1ns / 1ps


module top_tb( );

reg s_axi_clk=0;
reg  s_axi_reset=0;
localparam N_ADDR_BITS = 10, MEM_DEPTH = 1024;

reg [31:0] gpio_in=0;




reg [9:0] set_ram_addr = 0;
wire [9:0] ram_addr;
wire [7:0] ctrl_0;
wire ps_playback_done;
gpio_bridge u_gpio_bridge(
	.gpio_in       (gpio_in       ),
    .dout_0        (dout_0        ),
    .dout_1        (dout_1        ),
    .dout_2        (dout_2        ),
    .dout_3        (dout_3        ),
    .ctrl_0        (ctrl_0        ),
    .ctrl_1        (ctrl_1        ),
    .ctrl_2        (ctrl_2        ),
    .ctrl_3        (ctrl_3        ),
    .dout_readback (dout_readback )
);




mem_ctrl 
u_mem_ctrl(
	.s_axi_clk        (s_axi_clk        ),
    .playback_clk     (playback_clk     ),
    .s_axi_reset      (s_axi_reset      ),
    .set_ram_addr     (set_ram_addr     ),
    .ram_addr         (ram_addr         ),
    .write_oneshot    (write_oneshot    ),
    .ps_playback_done (ps_playback_done ),
    .wen              (wen              ),
    .dout_en          (dout_en          ),
    .din              (din              ),
    .ctrl             (ctrl_0             )
);

ram 
uut_ram (
	.s_axi_clk        (s_axi_clk        ),
.s_axi_reset      (s_axi_reset      ),
    .dout          (dout      ),
    .addr(ram_addr),
        .write_oneshot    (write_oneshot    ),
            .wen              (wen              ),
            .din(din)
);

/*
  assign ps_wen = gpio_in[31:28];
  assign ps_next_addr= gpio_in[27:24];
  assign ps_set_addr= gpio_in[23:20];
  assign ps_write= gpio_in[19:16];
  assign ps_mode= gpio_in[15:12];
  assign ps_playback_en= gpio_in[11:8];
  assign ps_dout_en = gpio_in[7:4];
  assign ps_din = gpio_in [3:0];
*/
initial begin
s_axi_reset = 0;
end

initial fork
#3 s_axi_reset = 1;
#5 gpio_in[12] = 1; // mode = write mode
//#7   gpio_in[28] = 1; //wen = 1;
#10 set_ram_addr = 14; //set target address

#12 gpio_in[20] = 1; //set address on
#14 gpio_in[20] = 0; //set address off
#15 gpio_in[0] = 0; //din = 0
#17 gpio_in[16]= 1; //write on 
#19 gpio_in[16]= 0; //write off

#30 $stop;
join

always #1 s_axi_clk = ~s_axi_clk;

endmodule