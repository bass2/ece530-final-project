// (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: rhit.org:rhit:mem_ctrl:1.0
// IP Revision: 7

`timescale 1ns/1ps

(* IP_DEFINITION_SOURCE = "package_project" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module rev5_rebuild_mem_ctrl_2_1 (
  s_axi_clk,
  playback_clk,
  s_axi_reset,
  set_ram_addr,
  ram_addr,
  write_oneshot,
  ps_playback_done,
  wen,
  dout_en,
  din,
  ctrl
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_clk, ASSOCIATED_RESET s_axi_reset, FREQ_HZ 250000000, PHASE 0.000, CLK_DOMAIN rev5_rebuild_processing_system7_0_0_FCLK_CLK0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s_axi_clk CLK" *)
input wire s_axi_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME playback_clk, FREQ_HZ 100000000, PHASE 0.000" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 playback_clk CLK" *)
input wire playback_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_reset, POLARITY ACTIVE_LOW" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s_axi_reset RST" *)
input wire s_axi_reset;
input wire [15 : 0] set_ram_addr;
output wire [15 : 0] ram_addr;
output wire write_oneshot;
output wire ps_playback_done;
output wire wen;
output wire dout_en;
output wire din;
input wire [7 : 0] ctrl;

  mem_ctrl #(
    .N_ADDR_BITS(16),
    .MEM_DEPTH(65536)
  ) inst (
    .s_axi_clk(s_axi_clk),
    .playback_clk(playback_clk),
    .s_axi_reset(s_axi_reset),
    .set_ram_addr(set_ram_addr),
    .ram_addr(ram_addr),
    .write_oneshot(write_oneshot),
    .ps_playback_done(ps_playback_done),
    .wen(wen),
    .dout_en(dout_en),
    .din(din),
    .ctrl(ctrl)
  );
endmodule
