`timescale 1ns / 1ps


 
    


module mem_tb( );

localparam ADDR_BITS = 10;
localparam DATA_DEPTH = 1024;
localparam DATA_WIDTH = 1;
localparam MEM_INIT_FILE =  "/home/adam/test.txt";
//inputs
reg s_axi_clk, s_axi_reset, dout_en,ps_wen, ps_next_addr, ps_set_addr, ps_write;
reg [ADDR_BITS - 1:0] set_ram_addr;
reg din;         
//outputs
 //wire write_oneshot;

 wire dout;
 
 wire [ADDR_BITS - 1:0] ram_addr;
mem_ctrl  mem_ctrl_uut (
        
 .s_axi_clk(s_axi_clk),
.s_axi_reset(s_axi_reset),
.ps_wen(ps_wen),
.ps_next_addr(ps_next_addr),
.ps_set_addr(ps_set_addr),
.ps_write(ps_write),
.set_ram_addr(set_ram_addr),
.ram_addr(ram_addr),
.write_oneshot(write_oneshot)

);



ram #(.MEM_INIT_FILE(MEM_INIT_FILE),.ADDR_BITS(ADDR_BITS),.DATA_WIDTH(DATA_WIDTH),.DATA_DEPTH(DATA_DEPTH))

ram_uut (
.s_axi_clk(s_axi_clk),
.s_axi_reset(s_axi_reset),
.wen(ps_wen),
.dout(dout),
.din(din),
.addr(ram_addr),
.write_oneshot(write_oneshot),
.dout_en(dout_en)
);

initial begin
    s_axi_clk = 0;
    s_axi_reset = 0;
    ps_wen = 0;
    ps_next_addr = 0;
    ps_set_addr = 0;
    ps_write = 0;
    set_ram_addr = 0;
    din = 0;
    dout_en = 0;
end

initial fork
    #4 s_axi_reset = 1;
    #5 din = 1;
   #7 set_ram_addr = 5;
   #9 ps_set_addr = 1;
   #12 ps_set_addr = 0;
    #15 ps_wen = 1;
    #19 ps_write = 1;
    #22 ps_write = 0;
    #25 ps_wen = 0;
    #26 dout_en = 1;
      #27 set_ram_addr = 9;
      #28 dout_en = 0;
     #29 ps_set_addr = 1;
     #32 ps_set_addr = 0;
     #33 ps_wen = 1;
     #34 ps_write = 1;
     #35 ps_write = 0;
     #38 ps_wen = 0;
     #41 dout_en = 1;
   #50 $stop;
    //#50 $stop;
join

always #1 s_axi_clk = ~s_axi_clk;




endmodule
