`timescale 1ns / 1ps
  module mem_ctrl#(parameter N_ADDR_BITS = 10, MEM_DEPTH = 1024)

   (
    input 		      s_axi_clk,
    input 		      playback_clk,
    input 		      s_axi_reset,

    input [N_ADDR_BITS - 1:0] set_ram_addr,
    output [N_ADDR_BITS -1:0] ram_addr,
    output 		      write_oneshot,
    output reg 		      ps_playback_done,
    output 		      wen,
    output 		      dout_en, din,
    input [7:0] 	      ctrl
    );
   localparam READ_MODE = 1'b0, WRITE_MODE = 1'b1;

   //assign dout_en = ctrl[7];


   assign dout_en = 1'bx;
      assign din = ctrl[6];
   assign mode = ctrl[5];

 assign wen = ctrl[4];
   assign ps_playback_en = ctrl[3];
   assign ps_next_addr = ctrl[2];
   assign ps_set_addr = ctrl[1];
   assign ps_write = ctrl[0];



   pos_oneshot set_addr_oneshot_unit(
				     .i_clk(s_axi_clk),
				     .i_reset(s_axi_reset),
				     .input_pulse(ps_set_addr),
				     .oneshot(set_addr_oneshot)
				     );


   pos_oneshot next_addr_oneshot_unit(
				      .i_clk(s_axi_clk),
				      .i_reset(s_axi_reset),
				      .input_pulse(ps_next_addr),
				      .oneshot(next_addr_oneshot)
				      );

   pos_oneshot write_oneshot_unit(
				  .i_clk(s_axi_clk),
				  .i_reset(s_axi_reset),
				  .input_pulse(ps_write),
				  .oneshot(write_oneshot)
				  );


   reg [N_ADDR_BITS - 1:0]    read_addr = 0;
   reg [N_ADDR_BITS - 1:0]    write_addr = 0;


   assign ram_addr = (mode == WRITE_MODE) ? write_addr : read_addr;






   //WRITE_MODE:

   always @(posedge s_axi_clk)
     begin

	if(s_axi_reset == 1'b0)
	  begin
	     write_addr <= 1'b0;
	  end

	else if(mode == WRITE_MODE && wen == 1'b1)
	  begin

 if(set_addr_oneshot == 1'b1)
	  begin
	     write_addr <= set_ram_addr;
	  end
	else
	  begin
	     write_addr <= write_addr;
	  end
     end
     end




   //READ_MODE:
   always @(posedge playback_clk or negedge s_axi_reset)
     begin
	if(s_axi_reset == 1'b0)
	  begin
	     read_addr <= 1'b0;
	     ps_playback_done <= 1'b1;
	  end

	else if(mode == READ_MODE ) begin
	   begin


	      if(playback_clk == 1'b1)
		begin


		   if(read_addr == MEM_DEPTH - 1'b1)
		     begin
			read_addr <= 1'b0;
			ps_playback_done <= 1'b1;
		     end
		   else begin
		      ps_playback_done <= 1'b0;
		      read_addr <= read_addr + 1'b1;
		   end


		end
	      else
		begin
		   read_addr <= read_addr;
		   ps_playback_done <= ps_playback_done;
		end

	   end

	end
     end



endmodule
