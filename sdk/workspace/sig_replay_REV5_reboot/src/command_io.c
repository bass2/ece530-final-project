/*
 * command_io.c
 *
 * Relies on cmdline.c and cmdline.h provided by T.I. developer
 * API.
 *
 * Contains functions supported terminal i/o for streamlining
 * development. These functions are only for parsing/handling
 * command line input relative to various functions.
 *
 *  	Created on: 1.20.19
 *      Author: Adam Satar
 */

#include "cmdline.h"
#include "command_io.h"
long cmd_val; /*Value passed in on the command line.*/
long cmd_val_2;
long cmd_val_3;

//Table of command string, function, and help string.

tCmdLineEntry g_sCmdTable[] = {
		{ "help", cmd_help, " : command arg1 arg2 arg3 " },
		{ "get_addr", cmd_get_addr, " : get_addr" },
		{ "set_addr", cmd_set_addr,	" : set_addr address" },
		{ "en_wram", cmd_enable_wram, " : en_wram" },
		{ "dis_wram", cmd_disable_wram, " : dis_wram" },
		{ "dump_ram", cmd_dump_ram, " : dump_ram" },
		{ "wram", cmd_wram, " : wram ch address value	" },
		{ "set_freq", cmd_set_freq, " : set_freq ch_num freq" },
		{ "dis_ch", cmd_disable_ch, " : dis_ch ch_num" },
		{ "en_ch", cmd_enable_ch, " : en_ch ch_num" },
		{ "set_play_mode", cmd_set_playback, " : set_play_mode ch_num" },
		{ "set_rec_mode", cmd_set_record, " : set_rec_mode ch_num" },
		{ "get_dout", cmd_get_dout, " : get_dout" },


		{ "play", cmd_play, " : play ch_mask" },
		{ "load", cmd_load, " : load ch_mask length" },
		{ "stop", cmd_stop, " : stop" },

		{ 0, 0, 0 } //end of table indicator
};

int cmd_set_playback(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); //channel
	set_playback_mode(cmd_val);
}
int cmd_set_record(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); //channel
	set_record_mode(cmd_val);
}

int cmd_set_freq(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); //channel
	cmd_val_2 = strtol(argv[2], 0, 10); //frequency
	set_freq(cmd_val, cmd_val_2);

	return 0;
}

int cmd_disable_ch(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); //channel
	disable_channel(cmd_val);
	return 0;
}

int cmd_enable_ch(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); //channel
	enable_channel(cmd_val);
	return 0;
}

int cmd_wram(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); // channel
	cmd_val_2 = strtol(argv[2], 0, 10); //address
	cmd_val_3 = strtol(argv[3], 0, 10); //value

	write_ram(cmd_val, cmd_val_2, cmd_val_3);

}

int cmd_dump_ram(int argc, char *argv[]) {
	// xil_printf("dumping ram...\n");
	cmd_val = strtol(argv[1], 0, 10); // channel
	dump_ram(cmd_val);
	return 0;
}

int cmd_set_addr(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); // channel
	cmd_val_2 = strtol(argv[2], 0, 10); // addr
	set_addr(cmd_val, cmd_val_2);
	return 0;
}

int cmd_enable_wram(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); // channel
	enable_wram(cmd_val);
	return 0;
}

int cmd_disable_wram(int argc, char *argv[]) {
	cmd_val = strtol(argv[1], 0, 10); // channel
	disable_wram(cmd_val);
	return 0;
}
int cmd_get_addr(int argc, char *argv[]) {
	xil_printf("current address is %d\n", get_addr());
	return 0;
}

int cmd_get_dout(int argc, char *argv[]) {
	xil_printf("%x", get_dout());
	return 0;
}


int cmd_get_ctrl_state(int argc, char *argv[]) {
	xil_printf("current CTRL state is 0x%x\n", get_ctrl_state());
	return 0;
}


int cmd_play(int argc, char *argv[]) {
	play(strtol(argv[1], 0, 16));
	return 0;
}

int cmd_load(int argc, char *argv[]) {
	load(strtol(argv[1], 0, 16), strtol(argv[2], 0, 10));
	return 0;
}

int cmd_stop(int argc, char *argv[]) {
	stop();
	return 0;
}


int cmd_help(int argc, char *argv[]) {
	tCmdLineEntry *pEntry;

	// Print some header text.
	xil_printf("\nAvailable commands\n");
	xil_printf("------------------\n");

	//
	// Point at the beginning of the command table.
	//
	pEntry = &g_sCmdTable[0];

	//
	// Enter a loop to read each entry from the command table.  The end of the
	// table has been reached when the command name is NULL.
	//
	while (pEntry->pcCmd) {
		//
		// Print the command name and the brief description.
		//
		xil_printf("%s%s\n", pEntry->pcCmd, pEntry->pcHelp);

		//
		// Advance to the next entry in the table.
		//
		pEntry++;
	}

	//
	// Return success.
	//
	return (0);
}

