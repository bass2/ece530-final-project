onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib rev5_rebuild_opt

do {wave.do}

view wave
view structure
view signals

do {rev5_rebuild.udo}

run -all

quit -force
