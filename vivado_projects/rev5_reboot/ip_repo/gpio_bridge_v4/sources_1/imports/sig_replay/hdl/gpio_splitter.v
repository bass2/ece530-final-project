`timescale 1ns / 1ps



module gpio_splitter(
            input [31:0] gpio_in,
            
    output[5:0] ctrl_0,ctrl_1,ctrl_2,ctrl_3,
    
    output din_0,din_1,din_2,din_3,
                 dout_en_0,dout_en_1,dout_en_2,dout_en_3
            
            

    );
    
    wire [3:0] ps_wen,ps_next_addr,ps_set_addr,ps_write,ps_mode,ps_playback_en;
    
       assign ps_wen = gpio_in[31:28];
       assign ps_next_addr= gpio_in[27:24];
       assign ps_set_addr= gpio_in[23:20];
       assign ps_write= gpio_in[19:16];
       assign ps_mode= gpio_in[15:12];
       assign ps_playback_en= gpio_in[11:8];

    
    
    
    

    assign din_0 = gpio_in[0];
    assign din_1 = gpio_in[1];
    assign din_2 = gpio_in[2];
    assign din_3 = gpio_in[3];
    
    assign dout_en_0 = gpio_in[4];
    assign dout_en_1 = gpio_in[5];
    assign dout_en_2 = gpio_in[6];
    assign dout_en_3     = gpio_in[7];
    

    




op_decoder_encoder op_dec_en

(
             .ps_wen(ps_wen),
             .ps_next_addr(ps_next_addr),
             .ps_set_addr(ps_set_addr),
             .ps_write(ps_write),
             .ps_mode(ps_mode),
             .ps_playback_en(ps_playback_en),

            .ctrl_0(ctrl_0),
            .ctrl_1(ctrl_1),
            .ctrl_2(ctrl_2),
            .ctrl_3(ctrl_3)
  );



endmodule
