connect -url tcp:127.0.0.1:3121
source /home/adam/temp/workspace/rev5_wrapper_hw_platform_REV5_REBOOT/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo 210279758982A"} -index 0
loadhw -hw /home/adam/temp/workspace/rev5_wrapper_hw_platform_REV5_REBOOT/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo 210279758982A"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo 210279758982A"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo 210279758982A"} -index 0
dow /home/adam/temp/workspace/REV5_REBOOT/Debug/REV5_REBOOT.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo 210279758982A"} -index 0
con
