`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//Adam Satar
//winter 1819
//ece team17
//parameterized ram module with output enable
//////////////////////////////////////////////////////////////////////////////////

module ram #(parameter DATA_WIDTH = 1, DATA_DEPTH = 1024,  
MEM_INIT_FILE =  "/home/adam/repos/sig_replay/mem_init_files/1024_10.txt", 
ADDR_BITS = 10)
(
        input s_axi_clk, 
                        wen,
                        write_oneshot,
                         dout_en,
                         s_axi_reset, 
                         [DATA_WIDTH - 1'b1:0] din,
        [ADDR_BITS -  1'b1 : 0] addr,
        output reg dout
    );
reg [DATA_WIDTH - 1'b1:0] ram [0:DATA_DEPTH - 1'b1];


initial begin
  if (MEM_INIT_FILE != "") begin
    $readmemh(MEM_INIT_FILE, ram);
  end
end

always @(posedge s_axi_clk) begin
    if(wen == 1'b0)
        dout <= ram[addr];
    else
        dout <= 1'bZ;
end

always @(posedge s_axi_clk) begin
    if(wen == 1'b1)
        if(write_oneshot == 1'b1)
            ram[addr] <= din;
        else 
            ram[addr] <= ram[addr];
end


endmodule
