`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//Adam Satar
//ece1819 team 17
//module to route  values from gpios to correct channel
//ultimately used for signal playback at various rates
//////////////////////////////////////////////////////////////////////////////////

    
module clk_gen_ctrl #(parameter SYSCLK = 100000000)
(input s_axi_clk,s_axi_reset,hs_clock,
input [1:0] ch_num,
input write,enable,
input [31:0] frequency,
output ch_0,ch_1,ch_2,ch_3                                      

    );
    

  localparam [1:0] CLK_0 = 2'b00, CLK_1 = 2'b01, CLK_2 = 2'b10, CLK_3 = 2'b11;
  
  reg en_0,en_1,en_2,en_3;
  reg wr_0,wr_1,wr_2,wr_3;
  always @(posedge s_axi_clk) begin
    case(ch_num)
    
        CLK_0: begin en_0 <= enable; wr_0 <= write;end
        CLK_1: begin en_1 <= enable; wr_1 <= write; end       
        CLK_2:begin  en_2 <= enable; wr_2 <= write;end
        CLK_3:begin en_3 <= enable; wr_3 <= write; end

      
    endcase
 end
    
    
clk_gen  #(.SYSCLK(SYSCLK)) clk_0_unit
    (.frequency(frequency),
  .s_axi_clk(s_axi_clk),
  .s_axi_reset(s_axi_reset),
  .hs_clock(hs_clock),
  .write(wr_0),
  .enable(en_0),
  .clk_out(ch_0)
  );


        clk_gen  #(.SYSCLK(SYSCLK)) clk_1_unit
    (.frequency(frequency),
  .s_axi_clk(s_axi_clk),
   .hs_clock(hs_clock),
  .s_axi_reset(s_axi_reset),
  .write(wr_1),
  .enable(en_1),
  .clk_out(ch_1)
  );
    


        clk_gen  #(.SYSCLK(SYSCLK)) clk_2_unit
    (.frequency(frequency),
  .s_axi_clk(s_axi_clk),
   .hs_clock(hs_clock),
  .s_axi_reset(s_axi_reset),
  .write(wr_2),
  .enable(en_2),
  .clk_out(ch_2)
  );
    
            clk_gen  #(.SYSCLK(SYSCLK)) clk_3_unit
(.frequency(frequency),
  .s_axi_reset(s_axi_reset),
 .hs_clock(hs_clock),
.write(wr_3),
.enable(en_3),
.clk_out(ch_3)
);

    
    

    
    
endmodule
