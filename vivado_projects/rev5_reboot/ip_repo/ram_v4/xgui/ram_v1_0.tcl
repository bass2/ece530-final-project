# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set ADDR_BITS [ipgui::add_param $IPINST -name "ADDR_BITS" -parent ${Page_0}]
  set_property tooltip {number of address bits} ${ADDR_BITS}
  set DATA_DEPTH [ipgui::add_param $IPINST -name "DATA_DEPTH" -parent ${Page_0}]
  set_property tooltip {size of memory} ${DATA_DEPTH}
  ipgui::add_param $IPINST -name "DATA_WIDTH" -parent ${Page_0}
  set MEM_INIT_FILE [ipgui::add_param $IPINST -name "MEM_INIT_FILE" -parent ${Page_0}]
  set_property tooltip {Memory initialization file. Each line is a new address. No delimiters or headers. Just separate contents line by line.} ${MEM_INIT_FILE}


}

proc update_PARAM_VALUE.ADDR_BITS { PARAM_VALUE.ADDR_BITS } {
	# Procedure called to update ADDR_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ADDR_BITS { PARAM_VALUE.ADDR_BITS } {
	# Procedure called to validate ADDR_BITS
	return true
}

proc update_PARAM_VALUE.DATA_DEPTH { PARAM_VALUE.DATA_DEPTH } {
	# Procedure called to update DATA_DEPTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DATA_DEPTH { PARAM_VALUE.DATA_DEPTH } {
	# Procedure called to validate DATA_DEPTH
	return true
}

proc update_PARAM_VALUE.DATA_WIDTH { PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to update DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DATA_WIDTH { PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to validate DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.MEM_INIT_FILE { PARAM_VALUE.MEM_INIT_FILE } {
	# Procedure called to update MEM_INIT_FILE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MEM_INIT_FILE { PARAM_VALUE.MEM_INIT_FILE } {
	# Procedure called to validate MEM_INIT_FILE
	return true
}


proc update_MODELPARAM_VALUE.DATA_WIDTH { MODELPARAM_VALUE.DATA_WIDTH PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DATA_WIDTH}] ${MODELPARAM_VALUE.DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.DATA_DEPTH { MODELPARAM_VALUE.DATA_DEPTH PARAM_VALUE.DATA_DEPTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DATA_DEPTH}] ${MODELPARAM_VALUE.DATA_DEPTH}
}

proc update_MODELPARAM_VALUE.MEM_INIT_FILE { MODELPARAM_VALUE.MEM_INIT_FILE PARAM_VALUE.MEM_INIT_FILE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MEM_INIT_FILE}] ${MODELPARAM_VALUE.MEM_INIT_FILE}
}

proc update_MODELPARAM_VALUE.ADDR_BITS { MODELPARAM_VALUE.ADDR_BITS PARAM_VALUE.ADDR_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ADDR_BITS}] ${MODELPARAM_VALUE.ADDR_BITS}
}

