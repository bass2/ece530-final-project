`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//Adam Satar
//1.18.19
//variable sqaure wave gen
//////////////////////////////////////////////////////////////////////////////////


module clk_gen #(parameter SYSCLK = 100000000)(
input s_axi_clk,s_axi_reset,write,enable,hs_clock,
input [31:0] frequency,
output reg clk_out

    );

    wire write_oneshot;
    pos_oneshot write_oneshot_unit(
                .i_clk(s_axi_clk),
                .i_reset(s_axi_reset),
                .input_pulse(write),
                .oneshot(write_oneshot)
    );
  reg [31:0] tic_count, half_period;
   always @(posedge s_axi_clk)
   if(s_axi_reset == 1'b0 || write_oneshot == 1'b1) 
        half_period <= SYSCLK/(frequency*2);
          
    always @(posedge hs_clock)
        if(s_axi_reset == 1'b0) begin 
                    tic_count <= 1'b0;
                    clk_out <= 1'b0; 
        end
            else if(enable == 1'b1) begin
             if(tic_count == half_period - 1'b1) begin
                    clk_out <= ~clk_out; 
                    tic_count <= 1'b0;
           end
            else tic_count <= tic_count + 1'b1;
    end
    else begin tic_count <= tic_count; clk_out <=0;end
        
    endmodule

