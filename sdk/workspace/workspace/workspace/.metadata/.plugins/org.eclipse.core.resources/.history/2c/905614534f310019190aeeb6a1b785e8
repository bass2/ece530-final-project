/*
 * main.c
 *
 *  Created on: Jan 28, 2019
 *      Author: Adam Satar
 *      ece1819 team17
 */

#include "xuartps_hw.h"
#include "xparameters.h"
#include "xgpio.h"
#include "xil_printf.h"
#include "cmdline.h"

#define CTRL_DEV_ID			XPAR_GPIO_0_DEVICE_ID
#define CTRL_DEV_CH			1
#define SET_ADDR_DEV_CH		2

// Macros to place a value each field of the CTRL register
#define CTRL_DIN(A)			((A) & 0xF) << 0
#define CTRL_DOUT_EN(A)		((A) & 0xF) << 4
#define CTRL_PLAYBACK_EN(A)	((A) & 0xF) << 8
#define CTRL_MODE(A)		((A) & 0xF) << 12
#define CTRL_MEM_WRITE(A)	((A) & 0xF) << 16
#define CTRL_SET_ADDR(A)	((A) & 0xF) << 20
#define CTRL_WRITE_EN(A)	((A) & 0xF) << 28

// CTRL_SET: Zeros the given field, and then sets it to the value argument
#define CTRL_SET(FIELD, VAL)			ctrl_state = ((ctrl_state & ~(FIELD(0xF))) | (FIELD(VAL)))
#define CTRL_SET_MASK(FIELD, VAL, MASK)	ctrl_state = ((ctrl_state & ~(FIELD(MASK))) | (FIELD(VAL & MASK)))
#define CTRL_SET_CH(FIELD, CH, VAL)		ctrl_state = ((ctrl_state & ~(FIELD(1 << CH))) | (FIELD(VAL << CH)))

#define FREQ_DEV_ID			XPAR_GPIO_1_DEVICE_ID
#define FREQ_CTRL_DEV_CH	1
#define FREQ_DEV_CH			2

#define DOUT_DEV_ID			XPAR_GPIO_2_DEVICE_ID
#define GET_ADDR_DEV_CH		1
#define DOUT_DEV_CH			2

#define GPIO_AS_OUTPUT		0
#define GPIO_AS_INPUT		1

#define DATA_DEPTH			1024
#define CMD_IO_BUF_LEN		256

#define PLAYBACK_MODE		0
#define RECORD_MODE			1

#define ENABLE 	1
#define DISABLE 0

u16 get_addr(void);
void set_addr(u8 ch_num, u16 address);
void enable_wram(u8);
void disable_wram(u8);
void clear_input_buffer(void);
void dump_ram(u8);
void set_freq(u8 ch_num, u32 frequency);
void enable_channel(u8 ch_num);
void disable_channel(u8 ch_num);
u16 get_dout();

void load(u8, u32);
void play(u8);
void stop(void);

void write_ctrl_state();
void set_record_mode(u8 ch_num);
void set_playback_mode(u8 ch_num);

void test_write_1024(void);
void test_load_sig(u16 address_offset);

XGpio Gpio_ctrl;
XGpio Gpio_dout;
XGpio Gpio_freq;

char input_buffer[CMD_IO_BUF_LEN];
u32 ctrl_state = 0;
u8  freq_ctrl_state = 0;

int main(void) {
	/*init devices*/
	XGpio_Initialize(&Gpio_ctrl, CTRL_DEV_ID);
	XGpio_Initialize(&Gpio_dout, DOUT_DEV_ID);
	XGpio_Initialize(&Gpio_freq, FREQ_DEV_ID);


	XGpio_SetDataDirection(&Gpio_ctrl, CTRL_DEV_CH, GPIO_AS_OUTPUT);
	XGpio_SetDataDirection(&Gpio_ctrl, SET_ADDR_DEV_CH, GPIO_AS_OUTPUT);

	XGpio_SetDataDirection(&Gpio_freq, FREQ_CTRL_DEV_CH, GPIO_AS_OUTPUT);
	XGpio_SetDataDirection(&Gpio_freq, FREQ_DEV_CH, GPIO_AS_OUTPUT);

	XGpio_SetDataDirection(&Gpio_dout, DOUT_DEV_CH, GPIO_AS_INPUT);
	XGpio_SetDataDirection(&Gpio_dout, GET_ADDR_DEV_CH, GPIO_AS_INPUT);

	XGpio_DiscreteWrite(&Gpio_ctrl, SET_ADDR_DEV_CH, 0);
	CTRL_SET(CTRL_MODE, 0xF); // Set all channels to RECORD mode
	CTRL_SET(CTRL_WRITE_EN, 0xF); // Set WRITE_EN for all channels
	CTRL_SET(CTRL_SET_ADDR, 0xF);
	write_ctrl_state();
	CTRL_SET(CTRL_SET_ADDR, 0x0);
	write_ctrl_state();

	char a;

	u8 buffer_index = 0;
//	xil_printf("entering main loop....\n");
	while (1) {
		a = XUartPs_RecvByte(STDIN_BASEADDRESS);
		if (a == '\n') {
			CmdLineProcess(input_buffer);
			xil_printf("\n");

			buffer_index = 0;
			clear_input_buffer();

		} else {
			input_buffer[buffer_index] = a;
			buffer_index++;
		}
	}

	xil_printf("end of main \n");

	return 0;
}

void write_ctrl_state()
{
	// xil_printf("\nsetting CTRL state %x\n", ctrl_state);
	XGpio_DiscreteWrite(&Gpio_ctrl, CTRL_DEV_CH, ctrl_state);
}

/****************************************************************************/
/**
 *
 * Put the specified channel in record mode. This enalbes writing to memory.
 *
 * @param	ch_num is the channel to put in record mode.
 * @return	None.
 *
 ****************************************************************************/
void set_record_mode(u8 ch_num) {
	xil_printf("\nsetting ch %d to RECORD MODE\n", ch_num);

	CTRL_SET_CH(CTRL_MODE, ch_num, RECORD_MODE);
	write_ctrl_state();
}
/****************************************************************************/
/**
 *
 *Put the specified channel in playback mode. The channel must be in playback mode
 *and playback must be enabled in order for the output to appear on the channel.
 *
 * @param	ch_num is the channel to disable playback on.
 * @return	None.
 *
 ****************************************************************************/
void set_playback_mode(u8 ch_num) {
	xil_printf("\nsetting ch %d to PLAYBACK MODE\n", ch_num);

	CTRL_SET_CH(CTRL_MODE, ch_num, PLAYBACK_MODE);
	write_ctrl_state();
}

/****************************************************************************/
/**
 *
 *Enable the clock that drives playback on the specified channel.
 *
 * @param	ch_num is the channel's clock that gets enabled
 * @return	None.
 *
 ****************************************************************************/
void enable_channel(u8 ch_num) {

	// enabling a channel before setting the frequency a big delay when
	// trying to enable the channel later (hardware issue)

	//xil_printf("enabling ch %d\n", ch_num);
	XGpio_DiscreteWrite(&Gpio_freq, FREQ_CTRL_DEV_CH, 0x4 | ch_num);
}

/****************************************************************************/
/**
 *
 *Disable the clock that drives playback on the specified channel.
 *
 * @param	ch_num is the channel's clock that gets disabled
 * @return	None.
 *
 ****************************************************************************/
void disable_channel(u8 ch_num) {
//	xil_printf("disabling  ch %d\n", ch_num);
	XGpio_DiscreteWrite(&Gpio_freq, FREQ_CTRL_DEV_CH, ch_num);
}

/****************************************************************************/
/**
 *
 * Dumps the contents of ram to data out line at whatever speed the ARM core does.
 * This has been observed to be around 100 Hz with all the print statements. The
 * frequency is irrelevant. This is just for testing and debugging r/w operations.
 *
 * @param	ch_num is the channel's clock that gets enabled
 * @return	None.
 *
 ****************************************************************************/
void dump_ram(u8 ch_num) {
	set_record_mode(ch_num);
	u16 i = 0;
	for (i = 0; i < DATA_DEPTH; i++) {
		set_addr(ch_num, i);
		xil_printf("dout: %x\n", get_dout());
	}
}

void clear_input_buffer(void) {
	u16 i = 0;

	for (i = 0; i < CMD_IO_BUF_LEN; i++) {
		input_buffer[i] = 0;
	}
}

void enable_wram(u8 ch_num) {
	CTRL_SET_CH(CTRL_WRITE_EN, ch_num, ENABLE);
	write_ctrl_state();

	xil_printf("wram enabled for ch %d\n", ch_num);
}
void disable_wram(u8 ch_num) {
	CTRL_SET_CH(CTRL_WRITE_EN, ch_num, DISABLE);
	write_ctrl_state();

	xil_printf("wram disabled for ch %d\n", ch_num);
}

u16 get_addr() {
	// We can only read the addr from channel 0 right now
	// Maybe get_addr just shouldn't exist?
	return XGpio_DiscreteRead(&Gpio_dout, GET_ADDR_DEV_CH);
}

void set_addr(u8 ch_num, u16 address) {

	//need to change this implementation but for now do these steps to set an address

	//enable wram
	enable_wram(ch_num);
	//set_record_mode
	set_record_mode(ch_num);

	// xil_printf("address is %d\n", get_addr());
	// xil_printf("setting address to %d\n", address);
	XGpio_DiscreteWrite(&Gpio_ctrl, SET_ADDR_DEV_CH, address);

	// Set addr oneshot
	CTRL_SET_CH(CTRL_SET_ADDR, ch_num, ENABLE);
	write_ctrl_state();
	CTRL_SET_CH(CTRL_SET_ADDR, ch_num, DISABLE);
	write_ctrl_state();

	//disable wram
//	disable_wram(ch_num);
	//put back in play mode
	//set_playback_mode(ch_num);
	// xil_printf("address set to %d\n", get_addr());
}

void set_addr_masked(u8 ch_mask, u16 address) {
	// Sets address for all channels in mask
	XGpio_DiscreteWrite(&Gpio_ctrl, SET_ADDR_DEV_CH, address);

	CTRL_SET(CTRL_SET_ADDR, ch_mask);
	write_ctrl_state();
	CTRL_SET(CTRL_SET_ADDR, 0);
	write_ctrl_state();
}

u16 get_dout()
{
	return XGpio_DiscreteRead(&Gpio_dout, DOUT_DEV_CH);
}

u32 get_ctrl_state()
{
	return ctrl_state;
}


void write_ram(u8 ch_num, u16 address, u8 value) {
	enable_wram(ch_num);
	xil_printf("writing %d to ch %d, address %d\n", value, ch_num, address);

	/* set addr */
	set_addr(ch_num, address);

	/*load din line with value*/
	CTRL_SET_CH(CTRL_DIN, ch_num, (value & 1));
	write_ctrl_state();

	/*trigger write oneshot*/
	CTRL_SET_CH(CTRL_MEM_WRITE, ch_num, ENABLE);
	write_ctrl_state();

	CTRL_SET_CH(CTRL_MEM_WRITE, ch_num, DISABLE);
	write_ctrl_state();
	disable_wram(ch_num);
}

void write_pattern_(u32 pattern, u16 times_to_repeat, u16 start_addr) {
	//xil_printf()
}


  void test_write_1024() {
	u8 i = 0;

	for (i = 0; i < 16; i++) {
		test_load_sig(i * 64);
	}
}


void test_load_sig(u16 address_offset) {

	u16 a = 0b1000010000100001;
	u16 b = 0b0000000000000000;
	u16 c = 0b1010100110010101;
	u16 d = 0b1111111111111111;

	u16 patterns[4];
	patterns[0] = a;
	patterns[1] = b;
	patterns[2] = c;
	patterns[3] = d;

	u16 addr = 0;

	u8 i, j =0;
	u16 temp = 0;
	for (i = 0; i < 4; i++) {
		temp = patterns[i];
		for (j = 0; j < 16; j++) {
			addr = address_offset + (i * 16) + j;
		//	xil_printf("addr = %d\n",addr);
			//xil_printf("val = %x\n",(temp >> j));
			write_ram(0, addr,(temp >> j)  & 0x0001);
			//xil_printf("%d\n",temp >> (addr%16) & 0x0001);

		}
	}
}


void write_ram_masked(u8 ch_mask, u8 values)
{
	CTRL_SET(CTRL_DIN, values);
	write_ctrl_state();

	/*trigger write oneshot*/
	CTRL_SET(CTRL_MEM_WRITE, ch_mask);
	write_ctrl_state();

	CTRL_SET(CTRL_MEM_WRITE, 0);
	write_ctrl_state();
}

/* =========================================================================
 * =========================================================================
 *                     Commands used by Python library
 * =========================================================================
 * =========================================================================
 * */

/****************************************************************************/
/**
 *
 *Sets the specified channel's frequency. Note that the actual playback will occur at half
 *the frequency value as address changes occur on the positive edge of the playback clock
 *signal.  Also note that there's no limit on the frequency value. More testing needs to be done
 *for finding what this limit is; parameters (noticed so far) that affect this range include
 *the number of playback clocks enabled and the physical pin location.
 *
 * @param	frequency is that of the playback clock.
 * @param	ch_num is the channel that gets the frequency set.
 * @return	None.
 *
 ****************************************************************************/

void set_freq(u8 ch_num, u32 frequency) {

	// xil_printf("setting ch %d to %d Hz\n", ch_num, frequency);

	XGpio_DiscreteWrite(&Gpio_freq, FREQ_DEV_CH, frequency);
	XGpio_DiscreteWrite(&Gpio_freq, FREQ_CTRL_DEV_CH, ch_num);

	XGpio_DiscreteWrite(&Gpio_freq, FREQ_CTRL_DEV_CH, 0x8 | ch_num);
	XGpio_DiscreteWrite(&Gpio_freq, FREQ_CTRL_DEV_CH, 0);
}

void load(u8 ch_mask, u32 length) {
	char c;

	for (u32 i=0; i<length; ++i) {
		c = XUartPs_RecvByte(STDIN_BASEADDRESS);

		set_addr_masked(ch_mask, i);
		write_ram_masked(ch_mask, c);
	}
}

void play(u8 ch_mask)
{
	set_addr_masked(ch_mask, 0);

	// Enable clocks
	for (u8 ch=0; ch<4; ++ch) {
		enable_channel(ch);
	}

	// Disable WRAM, Set playback mode
	CTRL_SET_MASK(CTRL_WRITE_EN, 0, ch_mask);
	CTRL_SET_MASK(CTRL_MODE, 0, ch_mask);
	write_ctrl_state();
}

void stop()
{
	CTRL_SET(CTRL_MODE, 0xF);
	CTRL_SET(CTRL_WRITE_EN, 0xF);
	write_ctrl_state();
}


