# get the directory where this script resides
set thisDir [file dirname [info script]]
# source common utilities
source -notrace $thisDir/utils.tcl

# create the directories to package the IP cleanly
if {![file exists ./cip]} {
   file mkdir ./cip
}
if {![file exists ./cip/bft]} {
   file mkdir ./cip/ram
}
if {![file exists ./cip/bft/bftLib]} {
   file mkdir ./cip/bft/ramLib
}
foreach f [glob ../hdl/ram.v] {
   file copy -force $f ./cip/ram/
}
# foreach f [glob ../hdl/bft/bftLib/*.vhdl] {
#    file copy -force $f ./cip/bft/bftLib/
# }

# Create project
create_project -force ram ./ram/ -part xc7z010clg400-1

add_files -norecurse [glob ./cip/ram/*.v*]
add_files -norecurse [glob ./cip/ram/ramLib/*.vhdl]
set_property library ramLib [get_files */round_*.vhdl]
set_property library ramLib [get_files */bft_package.vhdl]
set_property library ramLib [get_files */core_transform.vhdl]

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

ipx::package_project -root_dir ./cip/bft -library ip -vendor xilinx.com

# If successful, "touch" a file so the make utility will know it's done 
touch {.cip.done}
