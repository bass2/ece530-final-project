#
# Vivado(TM)
# htr.txt: a Vivado-generated description of how-to-repeat the
#          the basic steps of a run.  Note that runme.bat/sh needs
#          to be invoked for Vivado to track run status.
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
#

vivado -log rev5_rebuild_wrapper.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source rev5_rebuild_wrapper.tcl
