/*
 * command_io.h
 *
 *  Created on: 1.20.19
 *      Author: Adam Satar
 */

#ifndef COMMAND_IO_H_
#define COMMAND_IO_H_


// Command line functions


int cmd_set_freq(int argc, char *argv[]);
int cmd_disable_ch(int argc, char *argv[]);
int cmd_enable_ch(int argc, char *argv[]);

int cmd_help(int argc, char *argv[]);
int cmd_get_addr(int argc, char *argv[]);
int cmd_disable_wram(int argc, char *argv[]);
int cmd_enable_wram(int argc, char *argv[]);

int cmd_set_addr(int argc, char *argv[]);
int cmd_enable_wram(int argc, char *argv[]);
int cmd_enable_wram(int argc, char *argv[]);

int cmd_get_dout(int argc, char *argv[]);

int cmd_dump_ram(int argc, char *argv[]);
int cmd_wram(int argc, char *argv[]);

int cmd_get_ctrl_state(int argc, char *argv[]);

int cmd_set_playback(int argc, char *argv[]);
int cmd_set_record(int argc, char *argv[]);

int cmd_play(int argc, char *argv[]);
int cmd_load(int argc, char *argv[]);
int cmd_stop(int argc, char *argv[]);

#endif /* COMMAND_IO_H_ */
